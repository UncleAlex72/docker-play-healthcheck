FROM adoptopenjdk/openjdk11-openj9:alpine

MAINTAINER alex.jones@unclealex.co.uk

RUN apk update && apk add busybox-extras curl

HEALTHCHECK --interval=5m --retries=1 \
  CMD curl -S -s -H "Authorization: Bearer ${HEALTHCHECK_TOKEN}" -f "http://localhost:${VIRTUAL_PORT}${PROXY_SUBDIR}/health" || exit 1
